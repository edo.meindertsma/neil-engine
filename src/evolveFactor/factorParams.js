//Indicate which factors have what ranges

module.exports = [
  [ //Piece eval section
    [ //Pawn eval section
      {
        id: 'P',
        enabled: true, value: 1,
        min: 0, max: 1000, weight: 1
      },
      {
        id: 'Pa1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pa2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pa3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pa4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pa5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pa6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pa7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pa8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pb1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pb2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pb3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pb4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pb5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pb6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pb7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pb8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pc1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pc2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pc3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pc4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pc5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pc6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pc7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pc8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pd1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pd2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pd3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pd4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pd5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pd6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pd7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pd8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pe1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pe2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pe3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pe4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pe5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pe6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pe7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pe8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pf1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pf2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pf3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pf4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pf5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pf6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pf7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pf8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pg1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pg2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pg3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pg4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pg5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pg6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pg7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Pg8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ph1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ph2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ph3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ph4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ph5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ph6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ph7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ph8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      }
    ],
    [ //Bishop eval section
      {
        id: 'B',
        enabled: true, value: 3,
        min: 0, max: 1000, weight: 1
      },
      {
        id: 'Ba1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ba2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ba3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ba4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ba5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ba6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ba7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ba8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bb1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bb2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bb3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bb4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bb5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bb6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bb7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bb8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bc1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bc2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bc3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bc4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bc5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bc6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bc7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bc8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bd1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bd2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bd3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bd4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bd5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bd6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bd7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bd8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Be1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Be2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Be3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Be4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Be5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Be6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Be7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Be8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bf1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bf2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bf3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bf4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bf5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bf6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bf7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bf8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bg1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bg2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bg3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bg4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bg5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bg6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bg7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bg8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bh1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bh2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bh3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bh4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bh5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bh6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bh7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Bh8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      }
    ],
    [ //Knight eval section
      {
        id: 'N',
        enabled: true, value: 3,
        min: 0, max: 1000, weight: 1
      },
      {
        id: 'Na1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Na2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Na3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Na4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Na5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Na6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Na7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Na8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nb1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nb2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nb3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nb4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nb5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nb6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nb7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nb8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nc1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nc2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nc3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nc4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nc5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nc6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nc7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nc8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nd1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nd2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nd3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nd4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nd5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nd6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nd7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nd8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ne1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ne2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ne3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ne4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ne5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ne6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ne7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ne8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nf1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nf2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nf3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nf4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nf5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nf6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nf7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nf8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ng1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ng2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ng3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ng4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ng5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ng6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ng7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ng8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nh1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nh2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nh3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nh4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nh5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nh6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nh7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Nh8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      }
    ],
    [ //Rook eval section
      {
        id: 'R',
        enabled: true, value: 5,
        min: 0, max: 1000, weight: 1
      },
      {
        id: 'Ra1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ra2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ra3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ra4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ra5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ra6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ra7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ra8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rb1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rb2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rb3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rb4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rb5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rb6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rb7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rb8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rc1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rc2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rc3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rc4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rc5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rc6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rc7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rc8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rd1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rd2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rd3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rd4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rd5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rd6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rd7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rd8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Re1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Re2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Re3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Re4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Re5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Re6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Re7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Re8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rf1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rf2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rf3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rf4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rf5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rf6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rf7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rf8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rg1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rg2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rg3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rg4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rg5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rg6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rg7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rg8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rh1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rh2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rh3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rh4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rh5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rh6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rh7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Rh8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      }
    ],
    [ //Queen eval section
      {
        id: 'Q',
        enabled: true, value: 9,
        min: 0, max: 1000, weight: 1
      },
      {
        id: 'Qa1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qa2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qa3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qa4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qa5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qa6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qa7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qa8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qb1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qb2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qb3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qb4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qb5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qb6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qb7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qb8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qc1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qc2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qc3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qc4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qc5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qc6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qc7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qc8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qd1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qd2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qd3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qd4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qd5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qd6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qd7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qd8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qe1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qe2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qe3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qe4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qe5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qe6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qe7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qe8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qf1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qf2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qf3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qf4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qf5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qf6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qf7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qf8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qg1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qg2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qg3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qg4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qg5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qg6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qg7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qg8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qh1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qh2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qh3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qh4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qh5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qh6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qh7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Qh8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      }
    ],
    [ //Princess eval section
      {
        id: 'S',
        enabled: true, value: 7,
        min: 0, max: 1000, weight: 1
      },
      {
        id: 'Sa1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sa2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sa3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sa4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sa5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sa6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sa7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sa8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sb1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sb2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sb3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sb4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sb5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sb6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sb7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sb8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sc1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sc2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sc3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sc4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sc5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sc6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sc7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sc8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sd1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sd2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sd3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sd4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sd5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sd6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sd7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sd8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Se1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Se2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Se3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Se4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Se5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Se6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Se7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Se8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sf1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sf2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sf3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sf4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sf5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sf6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sf7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sf8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sg1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sg2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sg3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sg4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sg5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sg6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sg7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sg8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sh1',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sh2',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sh3',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sh4',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sh5',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sh6',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sh7',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Sh8',
        enabled: true, value: 0,
        min: -1000, max: 1000, weight: 1
      }
    ],
    [ //King eval section
      {
        id: 'K',
        enabled: false, value: 0,
        min: 0, max: 1000, weight: 1
      },
      {
        id: 'Ka1',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ka2',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ka3',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ka4',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ka5',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ka6',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ka7',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ka8',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kb1',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kb2',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kb3',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kb4',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kb5',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kb6',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kb7',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kb8',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kc1',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kc2',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kc3',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kc4',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kc5',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kc6',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kc7',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kc8',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kd1',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kd2',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kd3',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kd4',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kd5',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kd6',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kd7',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kd8',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ke1',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ke2',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ke3',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ke4',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ke5',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ke6',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ke7',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Ke8',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kf1',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kf2',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kf3',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kf4',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kf5',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kf6',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kf7',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kf8',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kg1',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kg2',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kg3',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kg4',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kg5',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kg6',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kg7',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kg8',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kh1',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kh2',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kh3',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kh4',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kh5',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kh6',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kh7',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      },
      {
        id: 'Kh8',
        enabled: false, value: 0,
        min: -1000, max: 1000, weight: 1
      }
    ]
  ]
].flat(Number.POSITIVE_INFINITY);